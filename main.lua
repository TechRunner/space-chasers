function Reset()
    GameOver = false
    playerX = width/2
    playerY = height/2
    playerVelX = 0
    playerVelY = 0
    playerState = 0
    EnamyCount = 0
    for i=0, EnamyCount, 1 do
        EnamyX[i] = math.random() * width
        EnamyY[i] = math.random() * height
        EnamyVelX[i] = 0
        EnamyVelY[i] = 0
    end
    playerScore = 0
end

function love.load()
    -- Basic Variables
    width = 320
    height = 240
    GamePaused = false
    GameOver = false
    Debug = false
    prevT = 0

    -- Audio
    music = love.audio.newSource("music.mp3", "stream")
    musicP = true
    captureSound = love.audio.newSource("capture.mp3", "static")


    -- Background
    starsX = {}
    starsY = {}
    starsC = 100
    for i = 0, starsC, 1 do
        starsX[i] = math.random() * width
        starsY[i] = math.random() * height
    end

    -- Controls
    MoveR = "right"
    MoveL = "left"
    MoveD = "down"
    MoveU = "up"

    -- Player Info
    playerX = width/2
    playerY = height/2
    playerSize = 5
    playerVelX = 0
    playerVelY = 0
    playerSpeed = 5
    playerState = 0
    playerScore = 0

    -- Enamy Info
    EnamySpeed = 2
    EnamyX = {}
    EnamyY = {}
    EnamyVelX = {}
    EnamyVelY = {}
    EnamyMaxSpeed = 2
    EnamyCount = 0
    EnamyWidth = 3
    EnamyDelay = 10
    EnamyTimeSince = 0
    for i=0, EnamyCount, 1 do
        EnamyX[i] = math.random() * width
        EnamyY[i] = math.random() * height
        EnamyVelX[i] = 0
        EnamyVelY[i] = 0
    end

    love.window.setMode(width, height, {fullscreen=false})
    love.audio.play(music)
end

function love.keypressed(k)
    -- Exit
    if k == "escape" then
        love.event.quit()
    -- Pause
    elseif k == "return" or k == "enter" then
        GamePaused = not GamePaused
        if GamePaused then
            print("Game Paused")
        elseif not GamePaused then
            print("Game UnPaused")
        end
    end
    if k == "=" and Debug then
        AddEnamy()
    end
    if k == "g" and Debug then
        GameOver = true
    end

    if k == "r" or k == "u" or k == "y" then
        Reset()
    end
    if k == "m" or k == "space" or k == "-" then
        if musicP then
            love.audio.pause(music)
            musicP = false
        else
            love.audio.play(music)
            musicP = true
        end
    end
end

function AddEnamy()
    newcount = EnamyCount + 1
    EnamyX[newcount] = math.random() * width
    EnamyY[newcount] = math.random() * height
    EnamyVelX[newcount] = 0
    EnamyVelY[newcount] = 0
    EnamyCount = EnamyCount + 1
    print("Adding Enamy")
end


function PlayerUpdate(dt)
    -- Left Right Movement
    if love.keyboard.isDown(MoveR) then
        playerVelX = playerVelX + (playerSpeed * dt)
    elseif love.keyboard.isDown(MoveL) then
        playerVelX = playerVelX - (playerSpeed * dt)
    end

    -- Up Down Movement
    if love.keyboard.isDown(MoveU) then 
        playerVelY = playerVelY - (playerSpeed * dt)
    elseif love.keyboard.isDown(MoveD) then
        playerVelY = playerVelY + (playerSpeed * dt)
    end
    
     -- Update Player
    playerX = playerX + playerVelX
    playerY = playerY + playerVelY
    if playerX > width then
        playerX = 0
    end
    if playerY > height then
        playerY = 0
    end
    if playerX < 0 then
        playerX = width
    end
    if playerY < 0 then
        playerY = height
    end
end

function EnamyUpdate(dt)
 -- Update Enamy
    for i = 0, EnamyCount, 1 do
        -- Left Right Movement
        if EnamyX[i] < playerX and EnamyVelX[i] < EnamyMaxSpeed then
            EnamyVelX[i] = EnamyVelX[i] + (EnamySpeed * dt)
        elseif EnamyX[i] > playerX and EnamyVelX[i] > -EnamyMaxSpeed then
            EnamyVelX[i] = EnamyVelX[i] - (EnamySpeed * dt)
        end

        -- Up Down Movement
        if EnamyY[i] < playerY and EnamyVelY[i] < EnamyMaxSpeed then
            EnamyVelY[i] = EnamyVelY[i] + (EnamySpeed * dt)
        elseif EnamyY[i] > playerY and EnamyVelY[i] > -EnamyMaxSpeed then
            EnamyVelY[i] = EnamyVelY[i] - (EnamySpeed * dt)
        end

        EnamyX[i] = EnamyX[i] + EnamyVelX[i]
        EnamyY[i] = EnamyY[i] + EnamyVelY[i]


        -- Boundry
        if EnamyX[i] > width then
            EnamyX[i] = 0
        elseif EnamyX[i] < 0 then
            EnamyX[i] = width
        end

        if EnamyY[i] > height then
            EnamyY[i] = 0
        elseif EnamyY[i] < 0 then
            EnamyY[i] = height
        end
    end
end


function love.update(dt)
    -- Update Enamy
    if not GamePaused and not GameOver then
        -- Update Player
        PlayerUpdate(dt)
        playerScore = playerScore + dt
        -- Update Enamy
        EnamyUpdate(dt)
        if math.floor(playerScore) % 10 == 0 and math.floor(playerScore) > prevT then
            AddEnamy()
            playerState = playerState + 1
        end
        prevT = math.floor(playerScore)
    end
       
    -- Pause
    if GamePaused and not GameOver then
        love.graphics.print("Game Paused")
    end

    -- Gameover
    if playerState == -1 then
        GameOver = true
    end
end

function Collision()
    PlayerWidth = 5 + playerState

    for i=0, EnamyCount, 1 do
        if playerX < EnamyX[i] + EnamyWidth and 
           playerX + PlayerWidth > EnamyX[i] and 
           playerY < EnamyY[i] + EnamyWidth and 
           playerY + PlayerWidth > EnamyY[i] then
            GameOver = true
        end
    end
end

function love.draw()
    -- Draw Background
    for i=0, starsC, 1 do
        love.graphics.rectangle("fill", starsX[i], starsY[i], 1, 1)
    end
    
    if not GameOver then
        -- Draw Player
        playerSize = 5 + playerState
        love.graphics.rectangle("fill", playerX, playerY, playerSize, playerSize)
        love.graphics.print(string.format("Score: %d", math.floor(playerScore)))
        love.graphics.print(string.format("Enamy Count: %d", EnamyCount+1),0,height-15)
    
        -- Draw Enamy
        for i=0, EnamyCount, 1 do
            love.graphics.rectangle("fill", EnamyX[i], EnamyY[i], EnamyWidth, EnamyWidth)
        end
        Collision()
    elseif GameOver then
        GameOver = true
        love.graphics.print("GAME OVER", width/2.1 - 30, height/2-20)
        love.graphics.print(string.format("High Score: %d", playerScore), width/2.7, height/1.95)
    end

    if GamePaused and not GameOver then
        love.graphics.print("Game Paused", width/2.1 - 30, height/2-20)
    end
end
