# Space Chasers
A long long time ago in a galaxy far far away A long spaceship is being bassieged by a volly of rogue missile comming from dimensional rifts in space time and trapped in an infinate loop of space. You are said pilot can you survive and how long?

# Rules
- Avaid being hit by the missiles tailing you
- Have fun


# Controls
- PC
  - Movement
    - Arrow Keys
  - Settings
    - Pause
      - Enter/Return
    - Mute Music
      - M key
    - Restart
      - R key
    - Quit
      - esc key
- Game Shell
  - Movement
    - D-Pad
  - Settings
    - Pause
      - Start Button
    - Mute Music
      - Xbox key map
        - Y Button
      - SNES key map
        - X Button
    - Restart
      - Select Button
    - Quit
      - MENU Button
